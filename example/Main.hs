{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedLabels  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RebindableSyntax  #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}

module Main where

import           Prelude hiding (null)

import           Control.Monad.Indexed
import qualified Data.HashMap.Strict                   as HashMap
import           Data.String                           (fromString)
import           GHC.OverloadedLabels                  (fromLabel)
import           Text.Printf

import           Database.Migreat
import           Database.Migreat.MigrationPlan
import qualified Database.Migreat.Postgres             as PSQL
import           Database.Migreat.SchemaState          (ColumnState (dataType),
                                                        TableState)
import           Database.Migreat.Statement.SQL        (printDataType,
                                                        printTableStatement)

showTable :: TableState PSQL.DataType -> String
showTable = HashMap.foldlWithKey'
  (\a k v -> printf "%s\n  %s: %s" a k (printDataType (dataType v)))
  ""

printStep :: MigrationStep PSQL.DataType -> IO ()
printStep step =
  printf "\nSQL: %s" (printTableStatement (statement step)) *> putStrLn
    ( HashMap.foldlWithKey' (\a k v -> printf "%s\n%s: %s" a k (showTable v))
                            ""
                            (schemaState step)
    )

allMigrations = do
  createTable #user_account $ do
    column #user_name $ do
      withType PSQL.Text
      notNull
    column #email $ do
      withType PSQL.Text
      null
    column #birthday $ do
      withType PSQL.Date
      notNull
  alterTable #user_account $ do
    addColumn #id         PSQL.Integer
    addColumn #first_name PSQL.Text
  alterTable #user_account $ do
    alterColumn #id         PSQL.Numeric
    alterColumn #first_name PSQL.Text
    -- indexed monads and do notation plz
  where a >> b = a >>>= const b

main :: IO ()
main = do
  putStrLn "Migration plan:"
  case buildCompleteMigrationPlan allMigrations of
    Just plan ->
      mapM_ printStep (migrationPlanToList plan)
    Nothing         -> putStrLn "Empty migration plan"
