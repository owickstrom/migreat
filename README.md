# Migreat

*Safe SQL schema migrations*

This is an experimental package, with the aim of providing type-safe and
fine-grain control migrations for SQL databases.

## Backlog

- [x] Type-safe migration language (first version)
- [ ] Migration steps
    - [x] Basic data structures
    - [x] Build steps from migration language
    - [ ] Read schema state from database
    - [ ] Find applicable subset of steps (> current state)
- [x] Proper SQL statement AST instead of strings
- [ ] First migration runner
- [ ] Validate coercions
- [ ] Nullability
- [ ] Defaults
- [ ] Queries
    - [ ] Intersperse `update`/`select into`
    - [ ] Temporary tables
- [ ] Type check against other definition (e.g. Persistent model)

## Examples

[Print Migration Plan Example](example/Main.hs)

Example output:

```bash
$ stack build ; stack exec migreat-example -- dbname=mydb
...
Schema state:
fromList [("event_log",fromList [("event",ColumnState {dataType = TypeJSONB}),("job_id",ColumnState {dataType = TypeText}),("time",ColumnState {dataType = TypeTimestampTZ})]),("deployment_job",fromList [("tag",ColumnState {dataType = TypeText}),("time",ColumnState {dataType = TypeTimestampTZ}),("result",ColumnState {dataType = TypeText}),("deployment_name",ColumnState {dataType = TypeText}),("error_message",ColumnState {dataType = TypeText}),("id",ColumnState {dataType = TypeText})]),("output_log",fromList [("job_id",ColumnState {dataType = TypeText}),("time",ColumnState {dataType = TypeTimestampTZ}),("output",ColumnState {dataType = TypeText})])]
Migration plan:

SQL: CREATE TABLE user_account(user_name TEXT, email TEXT);
user_account:
  email: TEXT
  user_name: TEXT

SQL: ALTER TABLE user_account(ADD COLUMN id BIGINT, ADD COLUMN first_name VARCHAR);
user_account:
  email: TEXT
  first_name: VARCHAR
  id: BIGINT
  user_name: TEXT

SQL: ALTER TABLE user_account(ALTER COLUMN id BIGINT, ALTER COLUMN first_name TEXT);
user_account:
  email: TEXT
  first_name: TEXT
  id: BIGINT
  user_name: TEXT
```

## License

Copyright 2018 Ⓒ Oskar Wickström

[Mozilla Public License Version 2.0](LICENSE)
