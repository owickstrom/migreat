The Type-Safe Migration Language
================================

.. toctree::

  hello-world
  creating-a-table-with-columns
  altering-an-existing-table
  type-safety
  sql-data-types