Creating a Table with Columns
=============================

Now that we know how to create an empty table, it’s time to level up and
specify the columns of a table. We want to create a table for storing
information about programming languages.

.. code-block:: haskell

    migration =
      createTable #programming_language $ do
        column #name           PSQL.Text
        column #first_released PSQL.Date
        column #creator        PSQL.Text

The first part, ``createTable #programming_language``, works the same as
before. Then, instead of ``noColumns``, we specify three columns in a
``do`` block;

-  ``name`` of type ``Text``,
-  ``first_released`` of type ``Date``, and
-  ``creator`` of type ``Text``.

The do block passed has type ``TableColumns``, but with different type
parameters than ``noColumns`` that we saw before.
