Hello World
===========

The following migration creates a table named ``hello_world`` with no
columns.

.. code-block:: haskell

    migration =
      createTable #hello_world noColumns

This is a complete migration that can be applied by Migreat! It doesn’t
do much, but it’s a start. So what’s going on here? Let’s walk through
each part.

-  We define the top-level binding ``migration``, whose value is our
   migration.
-  The ``createTable`` operation is analogous to ``CREATE TABLE`` in
   SQL. Given a table name and a definition of its columns, it creates a
   new table in your database.
-  The table name we give is written using a pound sign (``#``) prefix.
   That is, the name is *hello_world*, not *#hello_world*. The reason is
   that we use a GHC language extension called *OverloadedLabels*. It
   enables Migreat to know the name of the table *at compile-time*, in
   addition to at run-time. We’ll see shortly why this is important.
-  We specify ``noColumns`` as the column definitions for our new table.
   In later examples we will create table with columns, but let’s take
   it one step at a time.

In a GHCi session, we can inspect the type of the individual parts to
learn more. We begin by enabling the required language extension.

.. code-block:: haskell

    *Main> :set -XOverloadedLabels

Next, let’s construct a label.

.. code-block:: haskell

    *Main> :t #hello_world
    #hello_world :: IsLabel "hello_world" t => t

Using overloaded labels we get back some ``t`` with an ``IsLabel``
instance, parameterized by a ``Symbol``, the type-level string
``"hello_world"`` in our case. Moving on, we inspect the type of
``noColumns``.

.. code-block:: haskell

    *Main> :t noColumns
    noColumns
      :: TableColumns
           dt
           (NewTable Empty)
           (NewTable Empty)
           ()

We see that ``noColumns`` gives us a ``TableColumns`` for a new table,
where the new table is empty *before* and *after* the migration. This is
a central idea in Migreat; using types to describe the state of the
schema before and after a particular operation. Next, we inspect the
type of ``createTable``.

.. code-block:: haskell

    *Main> :t createTable
    createTable
      :: (KnownSymbol n, s .\ n) =>
         Name n
         -> TableColumns
              dt
              (NewTable Empty)
              (NewTable t)
              ()
         -> TableMigration
              dt
              (Schema s)
              (Schema (Extend n (ExistingTable t) s))
              ()

Here it gets a bit more complicated. Ignoring the constraints for now,
we focus on the arguments to ``createTable``.

-  The first argument is the table name, ``Name n``, where ``n`` is a
   known symbol at compile-time. The ``Name`` type has an instance of
   ``IsLabel``, and that is how we can use overloaded labels to
   construct a name value.
-  The second argument to ``createTable`` is a value of the
   ``TableColumns`` type, with some type parameters:

   -  The type ``dt`` represents the SQL data type of our particular
      database. For more information, see :doc:`sql-data-types`.
   -  The operation applies to a new table with no columns, represented
      by the type ``Table Empty``, and results in a new table with the
      columns specified by ``t``.
   -  The return type is ``()``.

-  The return type of ``createTable`` is a ``TableMigration``,
   parameterized by the same SQL data type as the columns, and two type
   parameters describing the effect of the operation:

   -  It will apply to a ``Schema s``.
   -  The operation results in a ``Schema`` extended by an
      ``ExistingTable`` named ``n``, where ``t`` are the columns from
      the ``TableColumns``.
   -  The return type is ``()``.

Let’s define the original migration in GHCi, and inspect its type.

.. code-block:: haskell

    *Main> :{
    *Main| migration =
    *Main|   createTable #hello_world noColumns
    *Main| :}
    *Main> :t migration
    migration
      :: (s Data.Row.Internal..\ "hello_world") =>
         TableMigration
           dt
           (Schema s)
           (Schema
              (Data.Row.Internal.Extend
                 "hello_world"
                 (ExistingTable Data.Row.Internal.Empty)
                 s))
           ()

The type-level information from column operations can be seen as
traveling up through the ``TableColumns`` and the ``NewTable``, into the
``ExistingTable`` named by ``createTable``, finally being merged into
the resulting ``Schema``. The result is a type-safe schema definition,
where illegal operations are caught by GHC’s excellent type system.
We’ll see examples of the type system helping us further along in this
document.
