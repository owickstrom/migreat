SQL Data Types
==============

Migreat aims to support multiple database management systems (DBMS), not
only PostgreSQL. Many mainstream DMBS has their own set of SQL data
types, extending the ANSI SQL set. In Migreat, each DBMS implementation
represents the supported SQL data types using distinct Haskell types.
These Haskell types are used throughout the migration language
operations.

Coercions
---------

.. todo:: Mappings between valid coercions