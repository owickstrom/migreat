Altering an Existing Table
==========================

.. todo:: Describe how to alter a table.

Example: Creating and Altering Tables
-------------------------------------

In the following example, we create a table ``user_account`` with three
columns, and then alter it, adding the additional ``id`` and
``first_name`` columns.

.. code-block:: haskell

    allMigrations = do
      createTable #user_account $ do
        column #user_name PSQL.Text
        column #email     PSQL.Text
        column #birthday  PSQL.Date
      alterTable #user_account $ do
        addColumn #id         PSQL.Integer
        addColumn #first_name PSQL.Text
