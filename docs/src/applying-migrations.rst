Applying Migrations
===================

Development Workflows
---------------------

Migrations often become problematic when working on multiple branches of
a codebase in parallel. Let’s say we start off with the follow sequence
of migrations; *A*, *B*, and *C*.

.. digraph:: starting_state
   :caption: The starting state, where we have three existing migrations.

    rankdir=LR;
    a[label="A"];
    b[label="B"];
    c[label="C"];

    a -> b;
    b -> c;

In our team, we decide to split up and work on separate features,
between Alice and Bob. Both work streams require changes to the database
schema, and so we end up with two branches of the codebase with
diverging migration sequences; Alice adding a migration *D*, and Bob
adding a migration *E*. They both run Migreat to apply their new
migrations.

.. digraph:: before_rebase
   :caption: Two branches with diverging migrations.

    rankdir=LR;
    a[label="A"];
    b[label="B"];
    c[label="C"];
    d[label="D",style=bold,fontname="bold"];
    e[label="E",style=bold,fontname="bold"];

    a -> b;
    b -> c;
    c -> d;
    c -> e;

Bob, having written migration *E*, is considered to be done with his
feature first. Alice, having written migration *D*, decides to *rebase*
her changes atop the other branch.

.. digraph:: before_rebase
   :caption: Alice’s migration code after rebasing atop Bob’s branch.

    rankdir=LR;
    a[label="A"];
    b[label="B"];
    c[label="C"];
    d[label="D",style=bold,fontname="bold"];
    e[label="E",style=bold,fontname="bold"];

    a -> b;
    b -> c;
    c -> e;
    e -> d;

The issue now is that Alice’s database is in a state based on her branch
*before* rebasing atop Bob’s branch. The *E* in between *C* and *D* has
not been applied to Alice's database, it has only been merged into the
migration code. To get around this situation, Alice can use the
``reapply-unsafe`` operation.

.. code-block:: console

    $ project-migration reapply-unsafe
    Database detected to be in state D, which is no longer valid.
    Rolling back migrations to C.
    Applying E.
    Applying D.

And there we go, Alice is back on track.