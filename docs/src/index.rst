*******
Migreat
*******

.. toctree::
  :hidden:

  introduction
  typesafe-migration-language/index
  applying-migrations
  todo