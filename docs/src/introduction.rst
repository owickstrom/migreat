Introduction
============

SQL database migration, in one form or another, is a necessity in most
projects. Looking at tools available for mainstream programming
languages, we find that some provide embedded domain-specific languages
(EDSL) for writing migration scripts, e.g. `Active
Record <http://guides.rubyonrails.org/active_record_basics.html#migrations>`__,
`Alembic <http://alembic.zzzcomputing.com/en/latest/index.html>`__, and
`Entity
Framework <https://docs.microsoft.com/en-us/aspnet/mvc/overview/getting-started/getting-started-with-ef-using-mvc/migrations-and-deployment-with-the-entity-framework-in-an-asp-net-mvc-application>`__.
Others let you write scripts as plain SQL, e.g.
`Flyway <https://flywaydb.org/>`__,
`Migratus <https://github.com/yogthos/migratus>`__, and
`postgresql-simple-migration <https://github.com/ameingast/postgresql-simple-migration>`__,
often by having one file per logical changeset.

In additional to adding or removing tables, columns, and constraints,
migrations often handle data already stored in the database. When
collapsing the *first name* and *last name* into a *full name* field in
your application, you might first create a new column, update all
existing rows to set the full name to the result of concatenating the
first and last name, and then drop the redundant columns.

Symmetrical Migrations
----------------------

With both EDSLs and plain SQL migrations, the up and down parts of a
migration must be symmetrical, in that the down part completely reverses
the effects of the up part, and vice versa. This property is enforced
only by programmer discipline, and if done poorly, can leave the
migration in an inconsistent state that has to be resolved manually. In
the case of Active Record, there is a *change* method, which lets you
describe the up migration and have Active Record try to figure out the
reverse automatically.

Schema and Application Code Consistency
---------------------------------------

With the previously mentioned tools, there is no guarantee that the
resulting database schema matches your applications data access. Tables,
columns, and constraints might be missing or incorrectly typed. One
approach to avoiding the mismatch is to use an object-relational mapper
(ORM), in object-oriented programming, and have it automatically migrate
the schema based on the definitions in your source code.

A notable tool in the Haskell ecosystem is
`Persistent <https://www.yesodweb.com/book/persistent>`__, which lets
you write Haskell-like data type declarations, including schema-specific
extensions for various constraints, and use the same type-safe
definition for data in your application code as for your database
schema.

The drawback is that you lose control. At migration-time, Persistent
knows only what state it needs to get the database into ultimately, and
based on the data types and current state of the database schema, it
tries to calculate a migration to apply. It supports adding new fields,
and changing types of fields (as long as the database supports the
resulting type coercion,) but cannot detect fields renames, and
considers removal of fields and tables unsafe. Moreover, data update and
insertion is not supported in Persistent migrations.

Ordering of Migrations
----------------------

Many of the available migration tools require a user-defined ordering of
migrations, often based on timestamps or a numeric versioning scheme,
for the migration tool to be able to track what migrations have been
applied. Such orderings have no real connection with the actual
migrations, and can cause issues when merging or rebasing different
branches of the codebase.

Say that you are working in a project using timestamps for migration
versioning, and that you rebase your development branch, including a new
database migration, on top of another branch also including a new
migration written just before yours. In this scenario, some tools will
not even recognize that you have “missed” one migration. In case you are
using an even simpler versioning scheme, you might have a version
collision.

Safety, Control, and Effect-Based Versioning
--------------------------------------------

What if we could combine the safety of typed definitions, used both in
application code and for schema migration, with the power of running
arbitrary DDL statements and data migrations using a sequence of SQL
files? Furthermore, what if migrations needed no manual versioning, and
were tracked by the migration tool based on their *effects*, rather than
an arbitrary version number.

These are the goals of *Migreat*.