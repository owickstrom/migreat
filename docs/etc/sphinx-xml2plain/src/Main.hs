{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Data.Char
import qualified Data.Text                 as T
import qualified Data.Text.IO              as TIO
import           Data.Tree.NTree.TypeDefs
import           System.Environment
import           System.IO
import           Text.Regex
import           Text.XML.HXT.Arrow.Pickle
import           Text.XML.HXT.Core         hiding (normalizeWhitespace)

import           Text.XML.HXT.HTTP

isBlank :: T.Text -> Bool
isBlank = (== T.empty) . T.strip

notSoSmartQuotes :: T.Text -> T.Text
notSoSmartQuotes =
  T.pack . (\s -> subRegex quoted s "\x201C\\1\x201D") . T.unpack
  where
    quoted = mkRegex "\"([^\"]+?)\""

normalizeWhitespace :: String -> String
normalizeWhitespace s = subRegex ws s " "
  where
    ws = mkRegex "\\s+"

getSectionText :: IOSArrow XmlTree T.Text
getSectionText =
  multi $
  (hasName "section" >>> getChildren)
  >>>
  (listA
   (getTitleText <+> getParagraphText <+> getBulletListText)
   >>> arr T.concat
   >>> isA (not . isBlank))
  where
    getTitleText =
      isElem
      >>> hasName "title"
      >>> getAllTextNormalized
    getParagraphText =
      isElem
      >>> hasName "paragraph"
      >>> getAllTextNormalized
    getBulletListText =
      listA
      (isElem
       >>> hasName "bullet_list"
       >>> getChildren
       >>> getListItemText)
      >>> arr (T.intercalate "\n")
    getListItemText =
      isElem
      >>> hasName "list_item"
      >>> getAllTextNormalized
      >>> isA (not . isBlank)
      >>> arr ("* " `T.append`)
    getAllTextNormalized =
      multi getText
      >>> arr (notSoSmartQuotes . T.pack . normalizeWhitespace)

main :: IO ()
main = do
  args <- getArgs
  case args of
    [inFile, outFile] -> do
        topChunks <- runX $ do
            readDocument [ withHTTP []
                         , withValidate no
                         , withCheckNamespaces no
                         ] inFile
            >>> getSectionText
        TIO.writeFile outFile (T.intercalate "\n\n" topChunks)
    _ ->
      TIO.hPutStrLn stderr "Usage: sphinx-xml2plain IN_FILE OUT_FILE"
