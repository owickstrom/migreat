# Maintainable Web Applications

This book is built using Sphinx and reStructured Text.

## Prerequisites

* [A TeX Distribution](https://www.latex-project.org/get/)
* [virtualenv](https://virtualenv.pypa.io/en/stable/)

## Setup

```bash
# Change to this directory in your terminal
cd maintainable-web-applications
virtualenv ENV
ENV/bin/activate
pip install -r requirements.txt
```

## Building HTML

```bash
make html
```

Or, a handy live-reloading server for when you are writing:

```bash
make livehtml
```

## Building the PDF

```bash
make latexpdf
```

## License

Copyright 2016-2017 &copy; Oskar Wickström

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>
