{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE KindSignatures         #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE TypeOperators          #-}

module Database.Migreat.Migration where

import           Control.Monad.Indexed
import           Control.Monad.Indexed.Free
import           Data.Row.Records

import           Database.Migreat.Name
import           Database.Migreat.Nullability

newtype Schema (tables :: Row *) =
  Schema (Rec tables)

data ColumnType dt =
  ColumnType

data ColumnNullability (n :: Nullability) = ColumnNullability

instance ToNullability (ColumnNullability Null) where
  toNullability _ = Null

instance ToNullability (ColumnNullability NotNull) where
  toNullability _ = NotNull

data Column (c :: Row *)

newtype NewTable (columns :: Row *) =
  NewTable (Rec columns)

newtype ExistingTable (columns :: Row *) =
  ExistingTable (Rec columns)

class ToUnityped ct dt | ct -> dt where
  toUnityped :: ct -> dt

data TableMigrationF dt i o a where
  CreateTable
    :: (KnownSymbol n, s .\ n)
    => Name n
    -> TableColumnsSpec dt (NewTable Empty) (NewTable t) ()
    -> a
    -> TableMigrationF dt (Schema s) (Schema (Extend n (ExistingTable t) s)) a
  AlterTable
    :: Name n
    -> ColumnMigration dt (s .! n) (ExistingTable t') ()
    -> a
    -> TableMigrationF dt (Schema s) (Schema (Modify n (ExistingTable t') s)) a

instance IxFunctor (TableMigrationF dt) where
  imap f (CreateTable n t a) = CreateTable n t (f a)
  imap f (AlterTable n t a) = AlterTable n t (f a)

type TableMigration dt = IxFree (TableMigrationF dt)

createTable
  :: (KnownSymbol n, s .\ n)
  => Name n
  -> TableColumnsSpec dt (NewTable Empty) (NewTable t) ()
  -> TableMigration
       dt
       (Schema s)
       (Schema (Extend n (ExistingTable t) s))
       ()
createTable name table = Free (CreateTable name table (Pure ()))

alterTable
  :: Name n
  -> ColumnMigration dt (s .! n) (ExistingTable t') ()
  -> TableMigration
       dt
       (Schema s)
       (Schema (Modify n (ExistingTable t') s))
       ()
alterTable name table = Free (AlterTable name table (Pure ()))

data TableColumnSpecF dt i o a where
  AddColumnProperty
    :: (KnownSymbol prop, c .\ prop)
    => Label prop
    -> value
    -> a
    -> TableColumnSpecF dt (Column c) (Column (Extend prop value c)) a

instance IxFunctor (TableColumnSpecF dt) where
  imap f (AddColumnProperty p v a) = AddColumnProperty p v (f a)

type TableColumnSpec dt = IxFree (TableColumnSpecF dt)

withType
  :: (c .\ "type", ToUnityped ct dt)
  => ct
  -> TableColumnSpec dt (Column c) (Column (Extend "type" ct c)) ()
withType ct = Free (AddColumnProperty (Label :: Label "type") ct (Pure ()))

null
  :: (c .\ "null")
  => TableColumnSpec
     dt
     (Column c)
     (Column (Extend "null" (ColumnNullability Null) c))
     ()
null = Free (AddColumnProperty (Label :: Label "null") (ColumnNullability :: ColumnNullability Null) (Pure ()))

notNull
  :: (c .\ "null")
  => TableColumnSpec
     dt
     (Column c)
     (Column (Extend "null" (ColumnNullability NotNull) c))
     ()
notNull = Free (AddColumnProperty (Label :: Label "null") (ColumnNullability :: ColumnNullability NotNull) (Pure ()))

columnSpecToRec
  :: TableColumnSpec dt (Column Empty) (Column c) ()
  -> Rec c
columnSpecToRec tcs = go tcs empty
  where
    go :: TableColumnSpec dt (Column c) (Column c') () -> Rec c -> Rec c'
    go (Pure _                 ) c = c
    go (Free (AddColumnProperty label value next)) c =
      go next (extend label value c)

data TableColumnsSpecF dt i o a where
  TableColumnSpec
    :: (c .! "type" ≈ ct, ToUnityped ct dt, c .! "null" ≈ cn, ToNullability cn)
    => Name n
    -> TableColumnSpec dt (Column Empty) (Column c) ()
    -> a
    -> TableColumnsSpecF dt (NewTable s) (NewTable (Extend n (Column c) s)) a

instance IxFunctor (TableColumnsSpecF dt) where
  imap f (TableColumnSpec n c a) = TableColumnSpec n c (f a)

type TableColumnsSpec dt = IxFree (TableColumnsSpecF dt)

noColumns :: TableColumnsSpec dt (NewTable Empty) (NewTable Empty) ()
noColumns = Pure ()

column
  :: (KnownSymbol n, c .! "type" ≈ ct, ToUnityped ct dt, c .! "null" ≈ cn, ToNullability cn)
  => Name n
  -> TableColumnSpec dt (Column Empty) (Column c) ()
  -> TableColumnsSpec
       dt
       (NewTable s)
       (NewTable (Extend n (Column c) s))
       ()
column name tcs = Free (TableColumnSpec name tcs (Pure ()))

data ColumnMigrationF dt i o a where
  AddColumn
    :: (KnownSymbol n, s .\ n, ToUnityped ct dt)
    => Name n
    -> ct
    -> a
    -> ColumnMigrationF dt (ExistingTable s) (ExistingTable (Extend n (ColumnType ct) s)) a
  AlterColumn
    :: (KnownSymbol n, ToUnityped ct dt)
    => Name n
    -> ct
    -> a
    -> ColumnMigrationF dt (ExistingTable t) (ExistingTable (Modify n (ColumnType ct) t)) a

instance IxFunctor (ColumnMigrationF dt) where
  imap f (AddColumn n c a) = AddColumn n c (f a)
  imap f (AlterColumn n c a) = AlterColumn n c (f a)

addColumn
  :: (KnownSymbol n, s .\ n, ToUnityped ct dt)
  => Name n
  -> ct
  -> ColumnMigration
       dt
       (ExistingTable s)
       (ExistingTable (Extend n (ColumnType ct) s))
       ()
addColumn name ct = Free (AddColumn name ct (Pure ()))

alterColumn
  :: (KnownSymbol n, ToUnityped ct dt)
  => Name n
  -> ct
  -> ColumnMigration
       dt
       (ExistingTable t)
       (ExistingTable (Modify n (ColumnType ct) t))
       ()
alterColumn name ct = Free (AlterColumn name ct (Pure ()))

type ColumnMigration dt = IxFree (ColumnMigrationF dt)
