module Database.Migreat.Statement where

import           Data.Text (Text)
import           Database.Migreat.Nullability

type Name = Text

data AlterTableStatement dt
  = AddColumn Name
              dt
  | AlterColumn Text
                dt
  deriving (Show, Eq)

data CreateTableStatement dt =
  Column { columnName :: Name, columnDataType :: dt, nullability :: Nullability }
  deriving (Show, Eq)

data TableStatement dt
  = CreateTable Name [CreateTableStatement dt]
  | AlterTable Name [AlterTableStatement dt]
  deriving (Show, Eq)
