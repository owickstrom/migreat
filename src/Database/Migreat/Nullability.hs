{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE PolyKinds              #-}
module Database.Migreat.Nullability where

data Nullability = Null | NotNull
  deriving (Show, Eq)

class ToNullability t where
  toNullability :: t -> Nullability
