{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module Database.Migreat.MigrationPlan where

import           Data.List.NonEmpty                   (NonEmpty (..))
import qualified Data.List.NonEmpty                   as NonEmpty
import           Data.Row.Records                     (Empty)

import           Database.Migreat.Migration
import           Database.Migreat.SchemaState
import           Database.Migreat.SchemaState.Builder (buildSchemaStates)
import           Database.Migreat.Statement           (TableStatement)
import           Database.Migreat.Statement.Builder   (buildSchemaStatements)

data MigrationStep dt = MigrationStep
  { statement   :: TableStatement dt
  , schemaState :: SchemaState dt
  } deriving (Eq, Show)

newtype MigrationPlan dt =
  MigrationPlan (NonEmpty (MigrationStep dt))
  deriving (Eq, Show)

migrationPlanToList :: MigrationPlan dt -> [MigrationStep dt]
migrationPlanToList (MigrationPlan steps) = NonEmpty.toList steps

data MigrationPlanError dt
  = SchemaStateMismatch (SchemaState dt) (SchemaState dt)
  deriving (Eq, Show)

buildCompleteMigrationPlan
  :: TableMigration dt (Schema Empty) (Schema s') () -> Maybe (MigrationPlan dt)
buildCompleteMigrationPlan migration = MigrationPlan
  <$> NonEmpty.nonEmpty steps
 where
  steps = zipWith MigrationStep
                  (buildSchemaStatements migration)
                  (buildSchemaStates migration)

onlyRemaining
  :: Eq dt
  => SchemaState dt
  -> MigrationPlan dt
  -> Either (MigrationPlanError dt) (MigrationPlan dt)
onlyRemaining state (MigrationPlan steps) =
  case NonEmpty.span (\s -> schemaState s /= state) steps of
    (unmatched, []) ->
      Left (SchemaStateMismatch state (schemaState (last unmatched)))
    (_, x:xs) -> Right (MigrationPlan (x :| xs))
