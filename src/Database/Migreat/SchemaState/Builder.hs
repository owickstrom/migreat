{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeOperators         #-}

module Database.Migreat.SchemaState.Builder
  ( buildSchemaStates
  ) where

import           Control.Monad.Indexed.Free
import qualified Data.HashMap.Strict          as HashMap
import           Data.Row.Records

import           Database.Migreat.Migration
import           Database.Migreat.Name
import           Database.Migreat.Nullability
import           Database.Migreat.SchemaState

createTableState
  :: TableColumnsSpec dt (NewTable t) (NewTable t') ()
  -> TableState dt
  -> TableState dt
createTableState (Pure _) table = table
createTableState (Free (TableColumnSpec name columnSpec next)) table =
  createTableState
    next
    ( HashMap.insert
      (nameToText name)
      ColumnState { dataType = toUnityped (r .! (Label :: Label "type"))
                  , nullability = toNullability (r .! (Label :: Label "null"))
                  }
      table
    )
  where
    r = columnSpecToRec columnSpec

buildTableState :: ColumnMigration dt i o () -> TableState dt -> TableState dt
buildTableState (Pure _                       ) table = table
buildTableState (Free (AddColumn name dt next)) table = buildTableState
  next
  (HashMap.insert (nameToText name) (ColumnState (toUnityped dt) NotNull) table)
buildTableState (Free (AlterColumn name dt next)) table = buildTableState
  next
  (HashMap.insert (nameToText name) (ColumnState (toUnityped dt) NotNull) table)

buildSchemaStates :: TableMigration dt (Schema Empty) s () -> [SchemaState dt]
buildSchemaStates migration = go migration mempty
 where
  go :: TableMigration dt i o () -> SchemaState dt -> [SchemaState dt]
  go (Pure _) _ = []
  go (Free (CreateTable name columns next)) schema =
    let key     = nameToText name
        table   = createTableState columns mempty
        schema' = HashMap.insert key table schema
    in  schema' : go next schema'
  go (Free (AlterTable name columns next)) schema =
    case HashMap.lookup key schema of
      Just table ->
        let schema' = HashMap.insert key (buildTableState columns table) schema
        in  schema' : go next schema'
      Nothing -> error "buildSchemaState: unexpected missing table state!"
    where key = nameToText name
