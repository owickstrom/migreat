module Database.Migreat.SchemaState where

import           Data.HashMap.Strict (HashMap)
import           Data.Text           (Text)
import           Database.Migreat.Nullability

type TableName = Text

type ColumnName = Text

type SchemaName = Text

data ColumnState dt = ColumnState
  { dataType :: dt
  , nullability :: Nullability
  } deriving (Show, Eq)

type TableState dt = HashMap ColumnName (ColumnState dt)

type SchemaState dt = HashMap TableName (TableState dt)

data SchemaStateReadError
  = UnsupportedDataType Text
  | UnexpectedError Text
  deriving (Eq, Show)
