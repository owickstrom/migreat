{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}

module Database.Migreat.Postgres
  ( DataType(..)
  , ColumnType(..)
  ) where

import           Database.Migreat.Migration (ToUnityped, toUnityped)
import           Database.Migreat.Statement.SQL

data DataType
  = TypeInteger
  | TypeNumeric
  | TypeText
  | TypeBool
  | TypeDate
  | TypeTimestampTZ
  | TypeJSONB
  deriving (Eq, Show)

data ColumnType (dt :: DataType) where
  Integer :: ColumnType TypeInteger
  Numeric :: ColumnType TypeNumeric
  Text :: ColumnType TypeText
  Bool :: ColumnType TypeBool
  Date :: ColumnType TypeDate
  TimestampTZ :: ColumnType TypeTimestampTZ
  JSONB :: ColumnType TypeJSONB

instance SqlDataType DataType where
  printDataType =
    \case
      TypeInteger -> "integer"
      TypeNumeric -> "numeric"
      TypeText -> "text"
      TypeBool -> "boolean"
      TypeDate -> "date"
      TypeTimestampTZ -> "timestamp with time zone"
      TypeJSONB -> "jsonb"

instance ToUnityped (ColumnType TypeInteger) DataType where
  toUnityped _ = TypeInteger

instance ToUnityped (ColumnType TypeNumeric) DataType where
  toUnityped _ = TypeNumeric

instance ToUnityped (ColumnType TypeText) DataType where
  toUnityped _ = TypeText

instance ToUnityped (ColumnType TypeBool) DataType where
  toUnityped _ = TypeBool

instance ToUnityped (ColumnType TypeDate) DataType where
  toUnityped _ = TypeDate

instance ToUnityped (ColumnType TypeTimestampTZ) DataType where
  toUnityped _ = TypeTimestampTZ

instance ToUnityped (ColumnType TypeJSONB) DataType where
  toUnityped _ = TypeJSONB
