{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Database.Migreat.Statement.SQL where

import           Data.Semigroup             ((<>))
import           Data.Text                  (Text)
import qualified Data.Text                  as Text
import           Database.Migreat.Nullability
import           Database.Migreat.Statement

class SqlDataType dt where
  printDataType :: dt -> Text

printAlterTableStatement :: SqlDataType dt => AlterTableStatement dt -> Text
printAlterTableStatement = \case
  AddColumn   name dt -> name <> " " <> printDataType dt
  AlterColumn name dt -> name <> " " <> printDataType dt

printNullability :: Nullability -> Text
printNullability = \case
  Null    -> "NULL"
  NotNull -> "NOT NULL"

printCreateTableStatement :: SqlDataType dt => CreateTableStatement dt -> Text
printCreateTableStatement = \case
  Column {..} ->
    columnName
      <> " "
      <> printDataType columnDataType
      <> " "
      <> printNullability nullability

printTableStatement :: SqlDataType dt => TableStatement dt -> Text
printTableStatement = \case
  CreateTable name stmts ->
    "CREATE TABLE "
      <> name
      <> "("
      <> Text.intercalate ", " (map printCreateTableStatement stmts)
      <> ");"
  AlterTable name stmts ->
    "ALTER TABLE "
      <> name
      <> "("
      <> Text.intercalate ", " (map printAlterTableStatement stmts)
      <> ");"
