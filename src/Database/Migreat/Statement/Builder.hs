{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Database.Migreat.Statement.Builder where

import           Control.Monad.Indexed.Free
import           Data.Row.Records

import           Database.Migreat.Migration
import           Database.Migreat.Name
import qualified Database.Migreat.Statement as Statement
import           Database.Migreat.Nullability

createTableStatements
  :: TableColumnsSpec dt (NewTable t) (NewTable t') ()
  -> [Statement.CreateTableStatement dt]
createTableStatements (Pure _) = []
createTableStatements (Free (TableColumnSpec name columnSpec next)) =
  let r = columnSpecToRec columnSpec
      stmt =
        Statement.Column
          { Statement.columnName     = nameToText name
          , Statement.columnDataType = toUnityped (r .! (Label :: Label "type"))
          , Statement.nullability    = toNullability (r .! (Label :: Label "null"))
          }
  in  stmt : createTableStatements next

alterTableStatements
  :: ColumnMigration dt i o () -> [Statement.AlterTableStatement dt]
alterTableStatements (Pure _) = []
alterTableStatements (Free (AddColumn name ct next)) =
  Statement.AddColumn (nameToText name) (toUnityped ct)
    : alterTableStatements next
alterTableStatements (Free (AlterColumn name ct next)) =
  Statement.AddColumn (nameToText name) (toUnityped ct)
    : alterTableStatements next

buildSchemaStatements
  :: TableMigration dt (Schema s) (Schema s') ()
  -> [Statement.TableStatement dt]
buildSchemaStatements (Pure _) = []
buildSchemaStatements (Free (CreateTable name columns next)) =
  Statement.CreateTable (nameToText name) (createTableStatements columns)
    : buildSchemaStatements next
buildSchemaStatements (Free (AlterTable name columns next)) =
  Statement.AlterTable (nameToText name) (alterTableStatements columns)
    : buildSchemaStatements next
