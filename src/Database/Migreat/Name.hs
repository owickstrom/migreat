{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels      #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module Database.Migreat.Name where

import           Data.Proxy
import           Data.Text            (Text)
import qualified Data.Text            as Text
import           GHC.OverloadedLabels (IsLabel, fromLabel)
import           GHC.TypeLits         (KnownSymbol, Symbol, symbolVal)

data Name (n :: Symbol) where
  Name :: KnownSymbol n => Name n

instance (KnownSymbol n, n ~ n') => IsLabel n (Name n') where
  fromLabel = Name :: Name n'

nameToString :: Name n -> String
nameToString (Name :: Name n) = symbolVal (Proxy :: Proxy n)

nameToText :: Name n -> Text
nameToText = Text.pack . nameToString
