{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module Database.Migreat.MigrationPlanTest where

import           Control.Monad.Indexed
import           Test.Tasty.Hspec

import           Database.Migreat.Migration
import           Database.Migreat.MigrationPlan (MigrationStep (..),
                                                 buildMigrationSteps)
import qualified Database.Migreat.Postgres      as PSQL
import           Database.Migreat.SchemaState
import qualified Database.Migreat.Statement     as Statement

a >>> b = a >>>= const b

spec_buildMigrationSteps =
  it "builds for multiple new tables" $ do
    let result =
          buildMigrationSteps $
          createTable
            #user_account
            (column #user_name PSQL.Text >>>
             column #email PSQL.Text) >>>
          createTable
            #account_provider
            (column #provider_id PSQL.Integer >>>
             column #provider_email PSQL.Text)
    result `shouldBe`
      [ MigrationStep
        { statement =
            Statement.CreateTable
              "user_account"
              [ Statement.Column "user_name" PSQL.TypeText
              , Statement.Column "email" PSQL.TypeText
              ]
        , schemaState =
            [ ( "user_account"
              , [ ("user_name", ColumnState PSQL.TypeText)
                , ("email", ColumnState PSQL.TypeText)
                ])
            ]
        }
      , MigrationStep
        { statement =
            Statement.CreateTable
              "account_provider"
              [ Statement.Column "provider_id" PSQL.TypeInteger
              , Statement.Column "provider_email" PSQL.TypeText
              ]
        , schemaState =
            [ ( "user_account"
              , [ ("user_name", ColumnState PSQL.TypeText)
                , ("email", ColumnState PSQL.TypeText)
                ])
            , ( "account_provider"
              , [ ("provider_id", ColumnState PSQL.TypeInteger)
                , ("provider_email", ColumnState PSQL.TypeText)
                ])
            ]
        }
      ]

{-# ANN module ("HLint: ignore Use camelCase" :: String) #-}
