{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module Database.Migreat.SchemaState.BuilderTest where

import           Control.Monad.Indexed
import           Test.Tasty.Hspec

import           Database.Migreat.Migration
import qualified Database.Migreat.Postgres            as PSQL
import           Database.Migreat.SchemaState
import           Database.Migreat.SchemaState.Builder (buildSchemaStates)

a >>> b = a >>>= const b

spec_builSchemaStates = do
  it "builds for new table" $ do
    let result =
          buildSchemaStates $
          createTable #user_account $
          column #user_name PSQL.Text >>>
          column #email PSQL.Text
    result `shouldBe`
      [ [ ( "user_account"
          , [ ("user_name", ColumnState PSQL.TypeText)
            , ("email", ColumnState PSQL.TypeText)
            ])
        ]
      ]
  it "builds for new table with alter" $ do
    let result =
          buildSchemaStates $
          createTable
            #user_account
            (column #user_name PSQL.Text >>>
             column #email PSQL.Text) >>>
          alterTable #user_account (alterColumn #user_name PSQL.JSONB)
    result `shouldBe`
      [ [ ( "user_account"
          , [ ("user_name", ColumnState PSQL.TypeText)
            , ("email", ColumnState PSQL.TypeText)
            ])
        ]
      , [ ( "user_account"
          , [ ("user_name", ColumnState PSQL.TypeJSONB)
            , ("email", ColumnState PSQL.TypeText)
            ])
        ]
      ]
  it "builds for multiple new tables" $ do
    let result =
          buildSchemaStates $
          createTable
            #user_account
            (column #user_name PSQL.Text >>>
             column #email PSQL.Text) >>>
          createTable
            #account_provider
            (column #provider_id PSQL.Integer >>>
             column #provider_email PSQL.Text)
    result `shouldBe`
      [ [ ( "user_account"
          , [ ("user_name", ColumnState PSQL.TypeText)
            , ("email", ColumnState PSQL.TypeText)
            ])
        ]
      , [ ( "user_account"
          , [ ("user_name", ColumnState PSQL.TypeText)
            , ("email", ColumnState PSQL.TypeText)
            ])
        , ( "account_provider"
          , [ ("provider_id", ColumnState PSQL.TypeInteger)
            , ("provider_email", ColumnState PSQL.TypeText)
            ])
        ]
      ]

{-# ANN module ("HLint: ignore Use camelCase" :: String) #-}
